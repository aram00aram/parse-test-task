<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Styles -->

</head>
<body>
    <form class="col-4 m-auto border p-3 mt-5" role="form" action="{{route('parse')}}" method="post">
        @csrf

        <div class="mb-3">
            <label for="inputPassword5" class="form-label">Url:</label>
            <input type="url" name="url" class="form-control" placeholder="link">
        </div>
        <div class="mb-3 text-end">
            <button class="btn btn-primary">Parse</button>
        </div>

    </form>

</body>
</html>
