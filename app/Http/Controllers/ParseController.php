<?php

namespace App\Http\Controllers;

use App\Service\Parse;
use Illuminate\Http\Request;

class ParseController extends Controller
{

    public function parse(Request $request, Parse $parse)
    {
        $res = $parse->parse($request);
        dd($res);
    }

}
