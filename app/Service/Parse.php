<?php

namespace App\Service;

use DOMDocument;

class Parse
{
    protected $data = [];

    protected $fillable = [
        [
            'tag' => 'img',
            'attr' => 'src',
        ],
        [
            'tag' => 'a',
            'attr' => 'href',
        ],
        [
            'tag' => 'link',
            'attr' => 'href',
        ],
        [
            'tag' => 'script',
            'attr' => 'src',
        ]
    ];

    public function parse($request)
    {
        $url = $request->url;
        $html = file_get_contents($url);
        $dom = new DOMDocument;
        @$dom->loadHTML($html);

        foreach ($this->fillable as $fillabl) {
            if(!empty($dom->getElementsByTagName($fillabl['tag']))){
                $links = $dom->getElementsByTagName($fillabl['tag']);
            }
            $this->getLinks($links, $fillabl);
        }

        return $this->data;
    }

    function getLinks($links, $fillabl)
    {
        foreach ($links as $link) {
            if(!empty( $link->getAttribute($fillabl['attr']))){
                $attrData[] = $link->getAttribute($fillabl['attr']);
            }
        }
        $this->data[$fillabl['tag']] = $attrData;
    }
}
